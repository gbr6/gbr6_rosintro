rosservice call /reset

rosservice call turtle1/set_pen 255 0 0 3 0

rosservice call turtle1/teleport_absolute -- 3 10 0

rosservice call /clear

rosservice call /spawn 4.5 8.8 0 '' 

rosservice call turtle2/set_pen 0 255 0 3 0

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[1.0,0,0.0]' '[0.0,0.0,0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[-0.9,2.9,0.0]' '[0.0,8,5.5]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-0.5,0.0,0.0]' '[0.0,0.0,0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-5.0,0.0,0.0]' '[0.0,0.0,4.5]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0,0.0,0.0]' '[0.0,0.0,-1.4]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.0,0.0,0.0]' '[0.0,0.0,0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0,0.5,0.0]' '[0.0,0.0,0.0]'
