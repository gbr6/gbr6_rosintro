#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/gbr6/catkin_ws/src/gbr6_rosintro/devel:$CMAKE_PREFIX_PATH"
export PWD='/home/gbr6/catkin_ws/src/gbr6_rosintro/build'
export ROSLISP_PACKAGE_DIRECTORIES="/home/gbr6/catkin_ws/src/gbr6_rosintro/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/gbr6/catkin_ws/src/gbr6_rosintro/src:$ROS_PACKAGE_PATH"